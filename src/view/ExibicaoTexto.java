package view;

import java.util.Scanner;
import java.util.ArrayList;
import model.*;

public class ExibicaoTexto {

	Scanner leitura;
	
	public ExibicaoTexto() {
		super();
	}
	
	public int ExibeMenu() {
		if (leitura == null) this.leitura = new Scanner(System.in);	
		int opc_menu = 0;
		boolean retornar = false;
		do {
			System.out.println("-----Venda de Livros-----");
			System.out.println("1 - Exibir informacao de livro");
			System.out.println("2 - Exibir informacao de venda de livro");
			System.out.println("3 - Exibir informacao de autor de livro");
			System.out.println("9 - Sair e ir para janela grafica!");
			opc_menu = leInt(leitura);
			switch(opc_menu) {
				case 1:
					retornar = true;
					break;
				case 2:
					retornar = true;
					break;
				case 3:
					retornar = true;
					break;
				case 9:
					retornar = true;
					break;
			}
		} while (!retornar);
		return opc_menu;
	}
	
	public void FechaMenu () {
		if (leitura != null) this.leitura.close();
		System.out.println("-----FIM-----");	
	}
	
	public void ExibeInformacao(ArrayList<Livro> livros) {
		System.out.println("-----Informacao de livro-----");
		System.out.println("Codigo do livro: ");
		int cod_livro = leInt(leitura);
		for(Livro l : livros){
	        if(l.getCodigo() == cod_livro) {
	    		System.out.printf("Codigo: %d\n", l.getCodigo() ); //chamando direto do model pq fiquei com pregui�a
	    		System.out.printf("Titulo: %s\n", l.getTitulo() ); 
	    		System.out.printf("Autor: %s\n", l.getAutor().getNome() );
	    		System.out.printf("Ano: %d\n", l.getAno() );
	    		System.out.printf("N. Paginas: %d\n", l.getPaginas() );
	    		System.out.printf("Isbn: %s\n", l.getIsbn() );
	    		return;
	        }
	    }
		System.err.println("Codigo inv�lido!");
	}
	
	public void ExibeInformacaoVenda(ArrayList<Livro> livros) {
		System.out.println("-----Informacao de venda de livro-----");
		System.out.println("Codigo do livro: ");
		int cod_livro = leInt(leitura);
		for(Livro l : livros){
	        if(l.getCodigo() == cod_livro) {
	    		System.out.printf("Codigo: %d\n", l.getCodigo() );
	    		System.out.printf("Titulo: %s\n", l.getTitulo() );
	    		System.out.printf("Autor: %s\n", l.getAutor().getNome() );
	    		System.out.printf("Preco: %.2f\n", l.getPreco() );
	    		if(l instanceof LivroEletronico){
		    		System.out.printf("Formato: %s\n", ((LivroEletronico) l).getFormato() );
	    		}
	    		else if(l instanceof LivroFisico){
		    		System.out.printf("Formato: %.2f\n", ((LivroFisico) l).getPeso() );
	    		}
	    		return;
	        }
	    }
		System.err.println("Codigo inv�lido!");
	}
	
	public void ExibeInformacaoAutor(ArrayList<Livro> livros) {
		System.out.println("-----Informacao de autor de livro-----");
		System.out.println("Codigo do livro: ");
		int cod_livro = leInt(leitura);
		for(Livro l : livros){
	        if(l.getCodigo() == cod_livro) {
	    		System.out.printf("Codigo: %d\n", l.getCodigo() );
	    		System.out.printf("Titulo: %s\n", l.getTitulo() );
	    		System.out.printf("Autor: %s\n", l.getAutor().getNome() );
	    		System.out.printf("Biografia: %s\n", l.getAutor().getBiografia() );
	    		return;
	        }
	    }
		System.err.println("Codigo inv�lido!");
	}	
	
	private int leInt(Scanner leitura) {
		int lido = 0;
		if (leitura.hasNextInt()) {
			lido = leitura.nextInt();
		} else {
			lido = 0;
		}
		leitura.nextLine(); // Consome nova linha do buffer
		return lido;
	}	

}
