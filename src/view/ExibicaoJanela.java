package view;

import java.util.ArrayList;
import model.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ExibicaoJanela extends JFrame implements ActionListener {

	JFrame janela;
	ArrayList<Livro> livros;
	
	public ExibicaoJanela(ArrayList<Livro> livros) {
		super();
		this.livros = livros;
	}
	
	public void ExibeMenu() {
		setBounds(50,50,600,270); 
		setTitle("Venda de livros");
		setLayout(null);
		JLabel label1 = new JLabel("Ol�, Bem-vindo(a) � Loja de Livros!");
		label1.setBounds(30,0,850,40);
		getContentPane().add(label1);
		
		JLabel label2 = new JLabel("Digite o c�digo do livro:");
		label2.setBounds(30, 40, 150, 20);
		getContentPane().add(label2);
		
		JTextField tf = new JTextField();
		tf.setBounds(200,40,50,20);
		tf.setText("0");
		
		getContentPane().add(tf);  
 
		JButton btInfLivro = new JButton("Exibir informacao do livro");
		btInfLivro.setBounds(30, 80, 300, 20);
		getContentPane().add(btInfLivro);
		
		btInfLivro.addActionListener(new ActionListener() 
		{ 
			@Override
			public void actionPerformed(ActionEvent e) 
			{ 
				int cod_livro = Integer.parseInt(tf.getText());
				for(Livro l : livros){
			        if(l.getCodigo() == cod_livro) {
						JOptionPane.showMessageDialog(null, 
							"Codigo: " + l.getCodigo() +
							"\nTitulo: " + l.getTitulo() +
							"\nAutor: " + l.getAutor().getNome() +
							"\nAno: " + l.getAno() +
							"\nN. p�ginas: " + l.getPaginas() +
							"\nIsbn: " + l.getIsbn()
							, "Informa��o do livro encontrado", JOptionPane.INFORMATION_MESSAGE);
			        }
				}
			}
		});
		
		JButton btInfVenda = new JButton("Exibir informacao de venda");
		btInfVenda.setBounds(30, 110, 300, 20);
		getContentPane().add(btInfVenda);
		
		btInfVenda.addActionListener(new ActionListener() 
		{ 
			@Override
			public void actionPerformed(ActionEvent e) 
			{ 
				int cod_livro = Integer.parseInt(tf.getText());
				for(Livro l : livros){
			        if(l.getCodigo() == cod_livro) {
						JOptionPane.showMessageDialog(null, 
							"Codigo: " + l.getCodigo() +
							"\nTitulo: " + l.getTitulo() +
							"\nAutor: " + l.getAutor().getNome() +
							"\nPreco: " + l.getPreco()
							, "Informa��o de venda encontrado", JOptionPane.INFORMATION_MESSAGE);
			        }
				}
			}
		});		
		
		JButton btInfAutor = new JButton("Exibir informacao do autor");
		btInfAutor.setBounds(30, 140, 300, 20);
		getContentPane().add(btInfAutor);
		
		btInfAutor.addActionListener(new ActionListener() 
		{ 
			@Override
			public void actionPerformed(ActionEvent e) 
			{ 
				int cod_livro = Integer.parseInt(tf.getText());
				for(Livro l : livros){
			        if(l.getCodigo() == cod_livro) {
						JOptionPane.showMessageDialog(null, 
							"Codigo: " + l.getCodigo() +
							"\nTitulo: " + l.getTitulo() +
							"\nAutor: " + l.getAutor().getNome() +
							"\nBiografia: " + l.getAutor().getBiografia()
							, "Informa��o do autor encontrado", JOptionPane.INFORMATION_MESSAGE);
			        }
				}
			}
		});		
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
