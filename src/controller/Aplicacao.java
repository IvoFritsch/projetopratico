package controller;

import java.util.ArrayList;

import model.*;
import view.*;

public class Aplicacao {

	public Aplicacao() {
		super();
	}
	
	public void VendaLivros() {

		Autor autor = new Autor();
		LivroEletronico livroe = new LivroEletronico();
		LivroFisico livrof = new LivroFisico();
		ArrayList<Livro> livros = new ArrayList<>();
		
		autor = new Autor("J. R. R. Tolkien", "lalala");
		livrof = new LivroFisico(1, "Senhor dos Aneis", 19.90, 1954, 1530, "9844564", autor, 450);
		livros.add(livrof);
		
		autor = new Autor("J. K. Rowling", "lelele");
		livroe = new LivroEletronico(2, "Harry Potter e a Pedra Filosofal", 15.90, 1997, 970, "54873389", autor, "PDF");
		livros.add(livroe);
		
		autor = new Autor("George R. R. Martin", "lololo");
		livrof = new LivroFisico(3, "As Cronicas de Gelo e Fogo", 18.90, 1996, 1240, "854756", autor, 350);
		livros.add(livrof);	
		
		
		ExibicaoTexto console = new ExibicaoTexto();
		int opc = 0;
		do {
			opc = console.ExibeMenu();
			switch(opc) {
				case 1:
					console.ExibeInformacao(livros);
					break;
				case 2:
					console.ExibeInformacaoVenda(livros);
					break;
				case 3:
					console.ExibeInformacaoAutor(livros);
					break;
			} 
		} while (opc != 9);
		console.FechaMenu();
		

		ExibicaoJanela janela = new ExibicaoJanela(livros);
		janela.ExibeMenu();
		
	}

}
