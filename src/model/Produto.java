package model;

public abstract class Produto {
	
	//private static int proximoCodigo = 0;

	private int codigo;
	private double preco;

	public Produto() {
	}
	
	public Produto(int codigo) {
		this.codigo = codigo;
		//this.codigo = proximoCodigo;
		//proximoCodigo++;
	}
	
	public double getPreco() {
		return preco;
	}
	
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public int getCodigo() {
		return codigo;
	}
}
