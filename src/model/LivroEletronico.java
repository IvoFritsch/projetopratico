package model;

public class LivroEletronico extends Livro {
	
	private String formato;

	public LivroEletronico() {
		super();
	}	
	
	public LivroEletronico(int codigo, String titulo, double preco, int ano, int paginas, String isbn, Autor autor, String formato) {
		super(codigo, titulo, preco, ano, paginas, isbn, autor);
		this.formato = formato;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

}
