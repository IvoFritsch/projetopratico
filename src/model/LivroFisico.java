package model;

public class LivroFisico extends Livro {

	private double peso;

	public LivroFisico() {
		super();
	}
	
	public LivroFisico(int codigo, String titulo, double preco, int ano, int paginas, String isbn, Autor autor, double peso) {
		super(codigo, titulo, preco, ano, paginas, isbn, autor);
		this.peso = peso;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

}
