package model;

public class Autor {

	private String nome;
	private String biografia;
	
	public Autor() {
		super();
	}
	
	public Autor(String nome, String biografia) {
		super();
		this.nome = nome;
		this.biografia = biografia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getBiografia() {
		return biografia;
	}

	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}

}
