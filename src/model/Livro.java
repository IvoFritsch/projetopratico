package model;

public class Livro extends Produto{
	
	private String titulo;
	private int ano;
	private int paginas;
	private String isbn;
	private Autor autor;

	public Livro() {
		super();
	}
	
	public Livro(int codigo, String titulo, double preco, int ano, int paginas, String isbn, Autor autor) {
		super(codigo);
		this.setPreco(preco);
		this.titulo = titulo;
		this.ano = ano;
		this.paginas = paginas;
		this.isbn = isbn;
		this.autor = autor;
	}	
	

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
}
